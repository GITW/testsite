package com.bbva.testng.maven;

import org.testng.annotations.Test;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;

public class TestRunner {
	WebDriver driver;


	@Test (description="Prueba 1")
	public void test0001() {
		driver = new FirefoxDriver();
		System.out.println("Se ejecuta la prueba test0001 ...");
		
		driver.get("http://www.bancomer.com");
		driver.manage().window().maximize();
		System.out.println("Finaliza la prueba test0001 ...");
		driver.close();
			
	}
	
	@Test (description="Prueba 2")
	public void test0002() {
		driver = new FirefoxDriver();
		System.out.println("Se ejecuta la prueba test0002 ...");
		driver.get("http://www.bancomer.com");
		driver.manage().window().maximize();
		System.out.println("Finaliza la prueba test0002 ...");
		driver.close();
			
	}
	


	@BeforeTest
	public void beforeTest() {
		
        

	}

	@AfterTest
	public void afterTest() {
		//driver.close();
	}

}
